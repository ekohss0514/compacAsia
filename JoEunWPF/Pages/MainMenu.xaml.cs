﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JoEunWPF.Common;

namespace JoEunWPF.Pages
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : UserControl, ISwitchable
    {
        public String Title = "조은그린영농조합";

        public MainMenu()
        {
            InitializeComponent();
        }

        #region ISwitchable Members;

        public void UtilizeState(Object state)
        {
            throw new NotImplementedException();
        }

        #endregion

        private void Menu_OnClick(object sender, RoutedEventArgs e)
        {
            var source = e.OriginalSource as FrameworkElement;

            if (source == null)
                return;

            var name = source.Name;
            switch(name) {
                case "BtnVent":
                    Switcher.Switch(new VentPage());
                    break;
                case "BtnGrower":
                    Switcher.Switch(new GrowerPage());
                    break;
                case "BtnReport":
                    Switcher.Switch(new ReportPage());
                    break;
                case "BtnReport2":
                    Switcher.Switch(new ReportPage2());
                    break;
            }
        }
    }
}
