﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.ComponentModel;
using JoEunWPF.Common;
using JoEunWPF.popup;

namespace JoEunWPF.Pages
{
    /// <summary>
    /// Interaction logic for GrowerPage.xaml
    /// </summary>
    public partial class GrowerPage : UserControl, ISwitchable, INotifyPropertyChanged
    {
        public String Title = "생산자 정보 관리";

        // 농가 데이터소스
        private CollectionViewSource growerViewSource;
        private CollectionViewSource groupViewSource;
        // 선택한 농가
        private GrowerModel _selectedGrower;
        public GrowerModel SelectedGrower
        {
            get
            {
                return _selectedGrower;
            }
            set
            {
                _selectedGrower = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("SelectedGrower"));
                }
            }
        }

        public GrowerPage()
        {
            InitializeComponent();
            this.DataContext = this;
            SelectedGrower = new GrowerModel();
            growerInfo.DataContext = SelectedGrower;
            searchGrowerList();
        }

        #region ISwitchable Members;

        public void UtilizeState(Object state)
        {
            throw new NotImplementedException();
        }

        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

            // Do not load your data at design time.
            // if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            // {
            // 	//Load your data here and assign the result to the CollectionViewSource.
            // 	System.Windows.Data.CollectionViewSource myCollectionViewSource = (System.Windows.Data.CollectionViewSource)this.Resources["Resource Key for CollectionViewSource"];
            // 	myCollectionViewSource.Source = your data
            // }

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                searchGroupList();
            }
            
        }

        // 그룹조회
        private void searchGroupList()
        {
            JoEunWPF.JoEunDataSet joEunDataSet = ((JoEunWPF.JoEunDataSet)(this.FindResource("joEunDataSet")));
            // Load data into the table Grower. You can modify this code as needed.
            JoEunWPF.JoEunDataSetTableAdapters.GrowerGroupTableAdapter joEunDataSetGrowerGroupTableAdapter = new JoEunWPF.JoEunDataSetTableAdapters.GrowerGroupTableAdapter();
            joEunDataSetGrowerGroupTableAdapter.Fill(joEunDataSet.GrowerGroup);
            groupViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("groupViewSource")));
            groupViewSource.View.MoveCurrentToFirst();
            growerDataGrid.UnselectAll();
        }
        // 농가조회
        private void searchGrowerList()
        {
            JoEunWPF.JoEunDataSet joEunDataSet = ((JoEunWPF.JoEunDataSet)(this.FindResource("joEunDataSet")));
            // Load data into the table Grower. You can modify this code as needed.
            JoEunWPF.JoEunDataSetTableAdapters.GrowerTableAdapter joEunDataSetGrowerTableAdapter = new JoEunWPF.JoEunDataSetTableAdapters.GrowerTableAdapter();
            joEunDataSetGrowerTableAdapter.FillBy(joEunDataSet.Grower, txtSearchWord.Text, txtSearchCode.Text);
            growerViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("growerViewSource")));
            growerViewSource.View.MoveCurrentToFirst();
            growerDataGrid.UnselectAll();
        }
        // 농가등록
        private void insertGrower(Object sender, RoutedEventArgs e)
        {
            // validation
            if (string.IsNullOrEmpty(SelectedGrower.name))
            {
                MessageBox.Show("생산자명을 입력해주세요.", "농가정보 누락", MessageBoxButton.OK);
                return;
            }
            MessageBoxResult result = MessageBox.Show("새로운 농가로 저장하시겠습니까?", "농가정보 등록", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.Cancel)
            {
                return;
            }

            JoEunWPF.JoEunDataSet joEunDataSet = ((JoEunWPF.JoEunDataSet)(this.FindResource("joEunDataSet")));
            // Load data into the table Grower. You can modify this code as needed.
            JoEunWPF.JoEunDataSetTableAdapters.GrowerTableAdapter joEunDataSetGrowerTableAdapter = new JoEunWPF.JoEunDataSetTableAdapters.GrowerTableAdapter();
            var resultCount = joEunDataSetGrowerTableAdapter.insertGrower(SelectedGrower.growerCode, SelectedGrower.name, SelectedGrower.tel, SelectedGrower.description, SelectedGrower.groupId);

            if (resultCount > 0)
            {
                // 재조회
                MessageBox.Show("저장되었습니다.", "농가정보 등록완료", MessageBoxButton.OK);
                SelectedGrower = new GrowerModel();
                growerInfo.DataContext = SelectedGrower;
                searchGrowerList();
            }
            else
            {
                MessageBox.Show("시스템 오류가 발생하였습니다. 동일증상 반복 발생시 시스템 관리자에게 문의하십시요.", "오류", MessageBoxButton.OK);
            }
        }
        // 농가수정
        private void modifyGrower(Object sender, RoutedEventArgs e)
        {
            // validation
            if (SelectedGrower.growerId < 1)
            {
                MessageBox.Show("수정하실 생산자를 선택해주세요.", "농가정보 누락", MessageBoxButton.OK);
                return;
            }
            if (string.IsNullOrEmpty(SelectedGrower.name))
            {
                MessageBox.Show("생산자명을 입력해주세요.", "농가정보 누락", MessageBoxButton.OK);
                return;
            }
            MessageBoxResult result = MessageBox.Show("변경된 정보를 저장하시겠습니까?", "농가정보 변경", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.Cancel)
            {
                return;
            }

            JoEunWPF.JoEunDataSet joEunDataSet = ((JoEunWPF.JoEunDataSet)(this.FindResource("joEunDataSet")));
            // Load data into the table Grower. You can modify this code as needed.
            JoEunWPF.JoEunDataSetTableAdapters.GrowerTableAdapter joEunDataSetGrowerTableAdapter = new JoEunWPF.JoEunDataSetTableAdapters.GrowerTableAdapter();
            var resultCount = joEunDataSetGrowerTableAdapter.UpdateGrower(SelectedGrower.groupId, SelectedGrower.growerCode, SelectedGrower.name, SelectedGrower.tel, SelectedGrower.description, SelectedGrower.growerId);

            if (resultCount > 0)
            {
                // 재조회
                MessageBox.Show("변경되었습니다.", "농가정보 수정완료", MessageBoxButton.OK);
                SelectedGrower = new GrowerModel();
                growerInfo.DataContext = SelectedGrower;
                searchGrowerList();
            }
            else
            {
                MessageBox.Show("시스템 오류가 발생하였습니다. 동일증상 반복 발생시 시스템 관리자에게 문의하십시요.", "오류", MessageBoxButton.OK);
            }
        }
        // 농가삭제
        private void deleteGrower(Object sender, RoutedEventArgs e)
        {
            if (SelectedGrower.growerId < 1)
            {
                MessageBox.Show("삭제하실 생산자를 선택해주세요.", "농가정보 누락", MessageBoxButton.OK);
                return;
            }
            MessageBoxResult result = MessageBox.Show("선택하신 농가를 삭제하시겠습니까?", "농가삭제", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.Cancel)
            {
                return;
            }

            JoEunWPF.JoEunDataSet joEunDataSet = ((JoEunWPF.JoEunDataSet)(this.FindResource("joEunDataSet")));
            // Load data into the table Grower. You can modify this code as needed.
            JoEunWPF.JoEunDataSetTableAdapters.GrowerTableAdapter joEunDataSetGrowerTableAdapter = new JoEunWPF.JoEunDataSetTableAdapters.GrowerTableAdapter();
            var resultCount = joEunDataSetGrowerTableAdapter.deleteGrower(SelectedGrower.growerId);

            if (resultCount > 0)
            {
                // 재조회
                MessageBox.Show("삭제되었습니다.", "농가삭제완료", MessageBoxButton.OK);
                SelectedGrower = new GrowerModel();
                growerInfo.DataContext = SelectedGrower;
                searchGrowerList();
            }
            else
            {
                MessageBox.Show("시스템 오류가 발생하였습니다. 동일증상 반복 발생시 시스템 관리자에게 문의하십시요.", "오류", MessageBoxButton.OK);
            }
        }
        // 조회버튼 클릭
        private void btnSearchOnClick(object sender, RoutedEventArgs e)
        {
            searchGrowerList();
        }
        // 생산자 선택
        private void growerClick(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                DataGridRow row = sender as DataGridRow;
                DataRowView drv = (DataRowView)row.Item;

                setGrowerInfo(drv);
            }
            else
            {
                MessageBox.Show("시스템 오류입니다. 관리자에게 문의하세요.");
            }
        }

        // 그룹변경
        private void OnGroupChanged(object sender, RoutedEventArgs e)
        {
            if (SelectedGrower != null && txtGroupId.SelectedValue != null)
            {
                SelectedGrower.groupId = Int32.Parse(txtGroupId.SelectedValue.ToString());
            }
        }

        // 농가정보셋팅
        private void setGrowerInfo(DataRowView drv)
        {
            SelectedGrower = new GrowerModel();
            // 순번, 이름, 비고, 그룹명, 그룹id, 생산자코드, 전화번호
            SelectedGrower.growerId = Int32.Parse(drv.Row[0].ToString());
            SelectedGrower.name = drv.Row[1].ToString();
            SelectedGrower.description = drv.Row[2].ToString();
            txtGroupId.SelectedValue = Int32.Parse(drv.Row[4].ToString());
            SelectedGrower.groupId = Int32.Parse(drv.Row[4].ToString());
            SelectedGrower.growerCode = drv.Row[5].ToString();
            SelectedGrower.tel = drv.Row[6].ToString();
            growerInfo.DataContext = SelectedGrower;
        }

        // 그룹편집 팝업
        private void openManageGroup(Object sender, RoutedEventArgs e)
        {
            // Instantiate the dialog box
            GroupPop dlg = new GroupPop();
            // Open the dialog box modally 
            dlg.ShowDialog();
            if (dlg.isUpdated)
            {
                searchGroupList();
                searchGrowerList();
                SelectedGrower = new GrowerModel();
                growerInfo.DataContext = SelectedGrower;
                growerDataGrid.UnselectAll();
            }
        }

        // 메인화면으로 가기
        private void goMain(object sender, RoutedEventArgs e)
        {
            growerDataGrid.UnselectAll();
            Switcher.Switch(new MainMenu());
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
