﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JoEunWPF.Common;

namespace JoEunWPF.Pages
{
    /// <summary>
    /// Interaction logic for ReportPage.xaml
    /// </summary>
    public partial class ReportPage2 : UserControl, ISwitchable
    {
        public String Title = "Reporting";

        public ReportPage2()
        {
            InitializeComponent();
            _reportViewer.Load += ReportViewer_Load;
        }

        private bool _isReportViewerLoaded;

        private void ReportViewer_Load(object sender, EventArgs e)
        {
            if (!_isReportViewerLoaded)
            {
                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new
                Microsoft.Reporting.WinForms.ReportDataSource();

                JoEunDataSet dataset = new JoEunDataSet();
                dataset.BeginInit();
                reportDataSource1.Name = "VentInfoDataSet";
                //Name of the report dataset in our .RDLC file
                reportDataSource1.Value = dataset.VentInfo;

                this._reportViewer.LocalReport.DataSources.Add(reportDataSource1);
                this._reportViewer.LocalReport.ReportPath = "../../ventINfo.rdlc";
                dataset.EndInit();

                JoEunDataSetTableAdapters.VentInfoTableAdapter VentInfoTableAdapter = new JoEunDataSetTableAdapters.VentInfoTableAdapter();
                VentInfoTableAdapter.Fill(dataset.VentInfo);
                _reportViewer.RefreshReport();
                _isReportViewerLoaded = true;

            }
        }

        #region ISwitchable Members;

        public void UtilizeState(Object state)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    
}
