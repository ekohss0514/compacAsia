﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JoEunWPF.Common;
using JoEunWPF.popup;

namespace JoEunWPF.Pages
{
    /// <summary>
    /// Interaction logic for ReportPage.xaml
    /// </summary>
    public partial class ReportPage : UserControl, ISwitchable
    {
        public String Title = "Reporting";
        public int growerId;

        private JoEunDataSet dataset;
        private JoEunDataSetTableAdapters.VentInfoTableAdapter VentInfoTableAdapter;

        public ReportPage()
        {
            InitializeComponent();
            // 오늘날짜 default
            txtStartDate.SelectedDate = DateTime.Today;     
            txtEndDate.SelectedDate = DateTime.Today;   
            
            _reportViewer.Load += ReportViewer_Load;

        }

        public bool _isReportViewerLoaded = false;

        private void ReportViewer_Load(object sender, EventArgs e)
        {
            if (!_isReportViewerLoaded)
            {
                Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
                dataset = new JoEunDataSet();
                dataset.BeginInit();
                reportDataSource1.Name = "VentInfoDataSet";
                //Name of the report dataset in our .RDLC file
                reportDataSource1.Value = dataset.VentInfo;
                dataset.EndInit();
                this._reportViewer.LocalReport.DataSources.Add(reportDataSource1);
                this._reportViewer.LocalReport.ReportPath = "./Resources/report/ventInfo.rdlc";

                VentInfoTableAdapter = new JoEunDataSetTableAdapters.VentInfoTableAdapter();
                _isReportViewerLoaded = true;
                // 디폴트 조회
                searchVentInfo();
            }
        }

        #region ISwitchable Members;

        public void UtilizeState(Object state)
        {
            throw new NotImplementedException();
        }

        #endregion

        // 조회버튼클릭
        public void searchOnClick(object sender, RoutedEventArgs e)
        {
            searchVentInfo();
        }

        // 조회
        private void searchVentInfo()
        {

            VentInfoTableAdapter.searchVentInfoList(dataset.VentInfo, growerId, txtStartDate.Text, txtEndDate.Text);
            _reportViewer.RefreshReport();
        }

        // 농가선택 팝업
        private void OpenGrowerPopup(object sender, RoutedEventArgs e)
        {

            // Instantiate the dialog box
            GrowerPop dlg = new GrowerPop();

            // Configure the dialog box

            // Open the dialog box modally 
            dlg.ShowDialog();
            Grower grower = dlg.SelectedGrower as Grower;

            if (grower != null)
            {
                growerId = grower.growerId;
                txtGrower.Text = grower.name;
            }
        }

        // 메인화면으로 가기
        private void goMain(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new MainMenu());
        }
    }

    
}
