﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Threading;
using System.Configuration;
using System.Reflection;
using JoEunWPF.Common;
using JoEunWPF.popup;

namespace JoEunWPF.Pages
{
    /// <summary>
    /// Interaction logic for VentPage.xaml
    /// </summary>
    public partial class VentPage : UserControl, ISwitchable
    {
        // 현재 배출정보
        private VentInfoModel CurrentVentInfo;
        // opc client
        public OPCCLient ole;
        // 정산시작 flag
        public Boolean isProgress = false;
        //
        private delegate void UpdateMyDelegatedelegate(int i);

        public VentPage()
        {
            // 컴포넌트 초기화
            InitializeComponent();
            // view model 생성
            CurrentVentInfo = new VentInfoModel();
            // view datacontext 지정
            VentInfoGrid.DataContext = CurrentVentInfo;
            // 버튼 초기화
            setButtonDisable();
            // opc client
            ole = new OPCCLient();
        }

        // 정산시작 스레드
        private void startVent()
        {
            isProgress = true;
            setButtonDisable();
            ThreadPool.QueueUserWorkItem(new WaitCallback(getVentInfo), 0);
        }
        // 정산종료 스레드
        private void stopVent()
        {
            isProgress = false;
            setButtonDisable();
            ThreadPool.QueueUserWorkItem(new WaitCallback(initVentInfo), 0);
        }

        // 배출구 배출정보 가져오기
        private void getVentInfo(Object stateInfo)
        {
            while (isProgress)
            {
                System.Threading.Thread.Sleep(2000);
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, ((Action)delegate
                {
                    var AppSettings = ConfigurationManager.AppSettings;

                    CurrentVentInfo.A_XXL_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["A_XXL_OPC"], 1));
                    CurrentVentInfo.A_XL_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["A_XL_OPC"], 1));
                    CurrentVentInfo.A_L_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["A_L_OPC"], 1));
                    CurrentVentInfo.A_M_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["A_M_OPC"], 1));
                    CurrentVentInfo.A_S_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["A_S_OPC"], 1));
                    CurrentVentInfo.A_SS_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["A_SS_OPC"], 1));
                    CurrentVentInfo.B_XXL_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["B_XXL_OPC"], 1));
                    CurrentVentInfo.B_XL_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["B_XL_OPC"], 1));
                    CurrentVentInfo.B_L_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["B_L_OPC"], 1));
                    CurrentVentInfo.B_M_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["B_M_OPC"], 1));
                    CurrentVentInfo.B_S_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["B_S_OPC"], 1));
                    CurrentVentInfo.B_SS_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["B_SS_OPC"], 1));
                    CurrentVentInfo.C_XXL_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["C_XXL_OPC"], 1));
                    CurrentVentInfo.C_XL_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["C_XL_OPC"], 1));
                    CurrentVentInfo.C_L_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["C_L_OPC"], 1));
                    CurrentVentInfo.C_M_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["C_M_OPC"], 1));
                    CurrentVentInfo.C_S_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["C_S_OPC"], 1));
                    CurrentVentInfo.C_SS_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["C_SS_OPC"], 1));
                    CurrentVentInfo.D_OPC = Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["D_OPC"], 1));
                }));
            }
        }

        // 배출구 메모리 초기화
        private void initVentInfo(Object stateInfo)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, ((Action)delegate
            {
                var AppSettings = ConfigurationManager.AppSettings;

                ole.writeWord(AppSettings["device"], AppSettings["A_XXL_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["A_XL_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["A_L_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["A_M_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["A_S_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["A_SS_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["B_XXL_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["B_XL_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["B_L_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["B_M_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["B_S_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["B_SS_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["C_XXL_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["C_XL_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["C_L_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["C_M_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["C_S_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["C_SS_OPC"], 1, 0);
                ole.writeWord(AppSettings["device"], AppSettings["D_OPC"], 1, 0);
            }));
        }

        // 공통 버튼 컨트롤
        private void setButtonDisable()
        {
            btnVentStart.IsEnabled = isProgress;
            btnVentStop.IsEnabled = !isProgress;
        }

        // 정산시작
        private void StartCount(object sender, RoutedEventArgs e)
        {
            // 생산자 선택 여부 확인
            if (string.IsNullOrEmpty(CurrentVentInfo.Name))
            {
               MessageBox.Show("선택하신 농가가 없습니다. 농가를 선택해주세요.");
               btnSelectGrower.Focus();
               return;
            }
            if (MessageBox.Show(String.Format("선택하신 농가는 [{0}] 입니다. 확인을 클릭하시면 이전 배출정보는 초기화됩니다. 작업을 진행하시겠습니까? ", CurrentVentInfo.Name), "정산시작확인", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
            {
                return;
            }
            // 배출정보 가져오기 시작
            startVent();
        }

        // 정산종료
        private void StopAccount(object sender, RoutedEventArgs e)
        {
            // 현재작업중인 생산자가 없는 경우 확인 메세지
            if (string.IsNullOrEmpty(CurrentVentInfo.Name))
            {
                MessageBox.Show(String.Format("현재 진행중인 농가정보가 없습니다. 농가를 선택해주세요.", CurrentVentInfo.Name), "오류", MessageBoxButton.OK);
                btnSelectGrower.Focus();
                return;
            }
            
            MessageBoxResult result = MessageBox.Show(String.Format("{0}님의 배출정보를 저장하시겠습니까?", CurrentVentInfo.Name), "저장", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                string Connection_String = ConfigurationManager.ConnectionStrings["JoEunWPF.Properties.Settings.JoEunConnectionString"].ConnectionString;
                // 저장
                JoEunWPF.JoEunDataSet joEunDataSet = ((JoEunWPF.JoEunDataSet)(this.FindResource("JoEunDataSet")));
                // Load data into the table Grower. You can modify this code as needed.
                JoEunWPF.JoEunDataSetTableAdapters.VentInfoTableAdapter joEunDataSetVentInfoTableAdapter = new JoEunWPF.JoEunDataSetTableAdapters.VentInfoTableAdapter();
                int returnValue = joEunDataSetVentInfoTableAdapter.InsertQuery(CurrentVentInfo.GrowerId, CurrentVentInfo.A_XXL, CurrentVentInfo.A_XL, CurrentVentInfo.A_L, CurrentVentInfo.A_M, CurrentVentInfo.A_S, CurrentVentInfo.A_SS,
                    CurrentVentInfo.B_XXL, CurrentVentInfo.B_XL, CurrentVentInfo.B_L, CurrentVentInfo.B_M, CurrentVentInfo.B_S, CurrentVentInfo.B_SS,
                    CurrentVentInfo.C_XXL, CurrentVentInfo.C_XL, CurrentVentInfo.C_L, CurrentVentInfo.C_M, CurrentVentInfo.C_S, CurrentVentInfo.C_SS,
                    CurrentVentInfo.D);

                // 저장성공
                if (returnValue > 0)
                {
                    MessageBox.Show("저장되었습니다.", "저장완료", MessageBoxButton.OK);
                    // ventInfo 초기화
                    CurrentVentInfo = new VentInfoModel();
                    // view datacontext 지정
                    VentInfoGrid.DataContext = CurrentVentInfo;
                    stopVent();
                }
                else
                {
                    MessageBox.Show("저장에 실패하였습니다. 관리자에게 문의하세요.", "오류", MessageBoxButton.OK);
                }
            }
        }

        // 농가선택 팝업
        private void OpenGrowerPopup(object sender, RoutedEventArgs e)
        {

            // 현재작업중인 생산자가 있는 경우 확인 메세지
            if (!string.IsNullOrEmpty(CurrentVentInfo.Name))
            {
                MessageBoxResult result = MessageBox.Show("현재 진행중인 농가가 존재합니다. 농가를 변경하시겠습니까?", "농가변경", MessageBoxButton.YesNo);
                
                if (result == MessageBoxResult.No)
                {
                    return;
                }
            }
            // Instantiate the dialog box
            GrowerPop dlg = new GrowerPop();

            // Configure the dialog box

            // Open the dialog box modally 
            dlg.ShowDialog();
            Grower grower = dlg.SelectedGrower as Grower;

            if (grower != null)
            {
                CurrentVentInfo.GrowerId = grower.growerId;
                CurrentVentInfo.Name = grower.name;
                txtSelectGrower.Text = grower.name;
            }
        }

        // 오차조정 -
        private void reCalcMinus(object sender, RoutedEventArgs e)
        {
            var bindingValue = ((Button)sender).Tag;
            if (bindingValue != null)
            {
                Type type = CurrentVentInfo.GetType();
                PropertyInfo pi = type.GetProperty(bindingValue.ToString());
                int currentOffset = (int) pi.GetValue(CurrentVentInfo);
                if (currentOffset > 0)
                {
                    pi.SetValue(CurrentVentInfo, currentOffset - 1);
                }
            }
        }

        // 오차조정 +
        private void reCalcPlus(object sender, RoutedEventArgs e)
        {
            var bindingValue = ((Button)sender).Tag;
            if (bindingValue != null)
            {
                Type type = CurrentVentInfo.GetType();
                PropertyInfo pi = type.GetProperty(bindingValue.ToString());
                int currentOffset = (int)pi.GetValue(CurrentVentInfo);
                pi.SetValue(CurrentVentInfo, currentOffset + 1);
            }
        }

        // 메인화면으로 가기
        private void goMain(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new MainMenu());
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region ISwitchable Members;

        public void UtilizeState(Object state)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
