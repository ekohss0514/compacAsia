﻿using System;
using System.Windows;
using System.Windows.Controls;
using JoEunWPF.Common;
using JoEunWPF.Pages;
using JoEunWPF.popup;

namespace JoEunWPF
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class PageSwitcher : Window
    {
        public PageSwitcher()
        {
            InitializeComponent();
            // Instantiate the dialog box
            SystemCheck systemCheck = new SystemCheck();
            // Open the dialog box modally 
            systemCheck.ShowDialog();
            
            switch(systemCheck.isNormal)
            {
                case 0:
                    MessageBox.Show("시스템 구성체크를 정상정으로 마치지 못했습니다. 시스템을 다시 시작해주십시요", "시스템 오류",  MessageBoxButton.OK);
                    this.Close();
                    break;
                case 1:
                    MessageBox.Show("OPC Server와 연결할 수 없습니다. 관리자에게 문의하세요", "시스템 오류",  MessageBoxButton.OK);
                    this.Close();
                    break;
                case 2:
                    MessageBox.Show("DB 연결을 찾을 수 없습니다. 관리자에게 문의하세요", "시스템 오류", MessageBoxButton.OK);
                    this.Close();
                    break;
                case 3 :
                    Switcher.pageSwitcher = this;
                    Switcher.Switch(new MainMenu());
                    break;
            }
        }

        public void Navigate(UserControl nextPage)
        {
            this.Content = nextPage;
        }

        public void Navigate(UserControl nextPage, object state)
        {
            this.Content = nextPage;
            ISwitchable s = nextPage as ISwitchable;

            if (s != null)
                s.UtilizeState(state);
            else
                throw new ArgumentException("NextPage is not ISwitchable! "
                  + nextPage.Name.ToString());
        }
    }
}