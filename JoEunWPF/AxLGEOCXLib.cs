//------------------------------------------------------------------------------
//Copyright 2011 LS industrial system
//LSIS OCX wrapper source for C# 
//------------------------------------------------------------------------------

[assembly: System.Windows.Forms.AxHost.TypeLibraryTimeStamp("2011-06-02 오전 10:49:38")]

namespace AxLGEOCXLib {
    
    
    [System.Windows.Forms.AxHost.ClsidAttribute("{5ecd72e6-3891-4b33-b3aa-80ef91da1f86}")]
    [System.ComponentModel.DesignTimeVisibleAttribute(true)]
    [System.ComponentModel.ToolboxItem(true)]
    public class AxLGEOCX : System.Windows.Forms.AxHost {
        
        protected LGEOCXLib._DLGEOCX ocx;

        protected AxLGEOCXEventMulticaster eventMulticaster;

        protected System.Windows.Forms.AxHost.ConnectionPointCookie cookie;
        
        public AxLGEOCX() : 
                base("5ecd72e6-3891-4b33-b3aa-80ef91da1f86") {
            this.SetAboutBoxDelegate(new AboutBoxDelegate(AboutBox));
        }
        
        public virtual void AboutBox() {
            if ((this.ocx == null)) {
                
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("AboutBox", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            this.ocx.AboutBox();
        }
        //current version don't use functions. Only for old verssion compablility.
        public virtual short InitDriver() {
            if ((this.ocx == null)) {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("InitDriver", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            short returnValue = ((short)(this.ocx.InitDriver()));
            return returnValue;
        }
        //current version don't use functions. Only for old verssion compablility.
        public virtual short OpenDriver(int nType) {
            if ((this.ocx == null)) {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("OpenDriver", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            short returnValue = ((short)(this.ocx.OpenDriver(nType)));
            return returnValue;
        }
        //current version don't use functions. Only for old verssion compablility.
        public virtual void CloseDriver() {
            if ((this.ocx == null)) {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("CloseDriver", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            this.ocx.CloseDriver();
        }
        
        public virtual void ReadData(string szDeviceName, string szAddress, short wType, int nDataLength, ref object pvTimeStamp, ref object pvQuality, ref object pvData) {
            if ((this.ocx == null)) {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("ReadData", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            this.ocx.ReadData(szDeviceName, szAddress, wType, nDataLength, ref pvTimeStamp, ref pvQuality, ref pvData);
        }
        
        public virtual short WriteData(string szDeviceName, string szAddress, short wType, int nDataLength, object  nData) {
            if ((this.ocx == null)) {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("WriteData", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            short returnValue = ((short)(this.ocx.WriteData(szDeviceName, szAddress, wType, nDataLength, nData)));
            return returnValue;
        }
        
        public virtual short GetDrvStatus(string szDrvName, ref int lStatus) {
            if ((this.ocx == null)) {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("GetDrvStatus", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            short returnValue = ((short)(this.ocx.GetDrvStatus(szDrvName, ref lStatus)));
            return returnValue;
        }
        
        public virtual short ReadData2(string szDeviceName, string szAddress, short wType, int nDataLength, ref object pvTimeStamp, ref object pvQuality, ref object pvData) {
            if ((this.ocx == null)) {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("ReadData2", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            short returnValue = ((short)(this.ocx.ReadData2(szDeviceName, szAddress, wType, nDataLength, ref pvTimeStamp, ref pvQuality, ref pvData)));
            return returnValue;
        }
        public virtual short SetEnable(string szDeviceName, int bEnbale)
        {
            if ((this.ocx == null))
            {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("SetEnable", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            short returnValue = ((short)(this.ocx.SetEnable(szDeviceName, bEnbale)));
            return returnValue;
        }

        public virtual short GetEnable(string szDeviceName)
        {
            if ((this.ocx == null))
            {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("GetEnable", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            short returnValue = ((short)(this.ocx.GetEnable(szDeviceName)));
            return returnValue;
        }
        public virtual short SetSimul(int bEnbale)
        {
            if ((this.ocx == null))
            {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("SetSimul", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            short returnValue = ((short)(this.ocx.SetSimul(bEnbale)));
            return returnValue;
        }

        public virtual short GetSimul()
        {
            if ((this.ocx == null))
            {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("GetSimul", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            short returnValue = ((short)(this.ocx.GetSimul()));
            return returnValue;
        }
        public virtual short Start()
        {
            if ((this.ocx == null))
            {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("Start", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            short returnValue = ((short)(this.ocx.Start()));
            return returnValue;
        }

        public virtual short Stop()
        {
            if ((this.ocx == null))
            {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("Stop", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            short returnValue = ((short)(this.ocx.Stop()));
            return returnValue;
        }

        public virtual short CommStatus()
        {
            if ((this.ocx == null))
            {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("CommStatus", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            short returnValue = ((short)(this.ocx.CommStatus()));
            return returnValue;
        }

        public virtual short LoadCfgFile(string szFileName)
        {
            if ((this.ocx == null))
            {
                throw new System.Windows.Forms.AxHost.InvalidActiveXStateException("LoadCfgFile", System.Windows.Forms.AxHost.ActiveXInvokeKind.MethodInvoke);
            }
            short returnValue = ((short)(this.ocx.LoadCfgFile(szFileName)));
            return returnValue;
        }
        
        protected override void CreateSink() {
            try {
                this.eventMulticaster = new AxLGEOCXEventMulticaster(this);
                this.cookie = new System.Windows.Forms.AxHost.ConnectionPointCookie(this.ocx, this.eventMulticaster, typeof(LGEOCXLib._DLGEOCXEvents));
            }
            catch (System.Exception ) {
            }
        }
        
        protected override void DetachSink() {
            try {
                this.cookie.Disconnect();
            }
            catch (System.Exception ) {
            }
        }
        
        protected override void AttachInterfaces() {
            try {
                this.ocx = ((LGEOCXLib._DLGEOCX)(this.GetOcx()));
            }
            catch (System.Exception ) {
            }
        }
    }
    
    [System.Runtime.InteropServices.ClassInterface(System.Runtime.InteropServices.ClassInterfaceType.None)]
    public class AxLGEOCXEventMulticaster : LGEOCXLib._DLGEOCXEvents {
        
        private AxLGEOCX parent;
        
        public AxLGEOCXEventMulticaster(AxLGEOCX parent) {
            this.parent = parent;
        }
    }
}
