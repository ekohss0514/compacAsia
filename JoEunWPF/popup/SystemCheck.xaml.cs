﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using System.Configuration;
using JoEunWPF.Common;

namespace JoEunWPF.popup
{
    /// <summary>
    /// Interaction logic for SystemCheck.xaml
    /// </summary>
    public partial class SystemCheck : Window
    {
        public int isNormal { get; set; }

        public SystemCheck()
        {
            InitializeComponent();
            double height = SystemParameters.WorkArea.Height;
            double width = SystemParameters.WorkArea.Width;
            this.Top = (height - this.Height) / 2;
            this.Left = (width - this.Width) / 2;   
            isNormal = 0;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(checkSystem), 0);
        }

        private void checkSystem(Object stateInfo)
        {
           System.Threading.Thread.Sleep(1000);
           this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, ((Action)delegate
           {
               // 1. check opc server
               Status.Text = "OPC Server 연결상태 확인중입니다.";
               try
               {
                   var AppSettings = ConfigurationManager.AppSettings;
                   // opc client
                   OPCCLient ole = new OPCCLient();
                   ole.CreateControl();
                   Convert.ToInt32(ole.readWord(AppSettings["device"], AppSettings["A_XXL_OPC"], 1));
               } catch(SystemException e) {
                   isNormal = 1;
                   Status.Text = "OPC Server에 연결하지 못했습니다.";
                   this.Close();
                   return;
               }

               // 2. check db connection
               Status.Text = "DB 연결상태 확인중입니다.";
               try
               {
                   JoEunWPF.JoEunDataSet joEunDataSet = ((JoEunWPF.JoEunDataSet)(this.FindResource("JoEunDataSet")));
                   JoEunDataSetTableAdapters.GrowerTableAdapter joEunDataSetGrowerTableAdapter = new JoEunWPF.JoEunDataSetTableAdapters.GrowerTableAdapter();
                   joEunDataSetGrowerTableAdapter.Fill(joEunDataSet.Grower);                 
               }
               catch (SystemException e)
               {
                   Status.Text = "DB에 접속할 수 없습니다.";
                   isNormal = 2;
                   this.Close();
                   return;
               }
               isNormal = 3;
               this.Close();
           }));
           

        }
    }
}
