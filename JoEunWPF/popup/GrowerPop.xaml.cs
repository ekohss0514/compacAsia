﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlServerCe;

namespace JoEunWPF.popup
{
    /// <summary>
    /// Interaction logic for GrowerPop.xaml
    /// </summary>
    public partial class GrowerPop : Window
    {
        // 선택한 생산자
        private Grower _selectedGrower;
        public Grower SelectedGrower
        {
            get
            {
                return _selectedGrower;
            }
        }

        // viewsource
        private CollectionViewSource growerViewSource;
        // dataset
        private JoEunDataSet joEunDataSet;
        private JoEunDataSetTableAdapters.GrowerTableAdapter joEunDataSetGrowerTableAdapter;

        // 생성자
        public GrowerPop()
        {
            // 초기화
            InitializeComponent();
            double height = SystemParameters.WorkArea.Height;
            double width = SystemParameters.WorkArea.Width;
            this.Top = (height - this.Height) / 2;
            this.Left = (width - this.Width) / 2;   
            // data context 설정
            this.DataContext = this;

            joEunDataSet = ((JoEunWPF.JoEunDataSet)(this.FindResource("JoEunDataSet")));
            // Load data into the table Grower. You can modify this code as needed.
            joEunDataSetGrowerTableAdapter = new JoEunWPF.JoEunDataSetTableAdapters.GrowerTableAdapter();
        }

        // 윈도우 로드
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            searchGrower("");
        }

        // 성 문자 클릭
        private void searchByName(object sender, RoutedEventArgs e)
        {
            var bindingValue = ((Button)sender).Tag;
            if (bindingValue != null)
            {
                searchGrower(bindingValue.ToString());
            }
        }

        // 생산자목록 조회
        private void searchGrower(string name)
        {
            joEunDataSetGrowerTableAdapter.selectGrowerByName(joEunDataSet.Grower, name);
            growerViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("growerViewSource")));
            growerViewSource.View.MoveCurrentToFirst();
            growerDataGrid.UnselectAll();
        }

        // 생산자 선택시
        private void grower_doubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                DataGridRow row = sender as DataGridRow;
                DataRowView drv = row.Item as DataRowView;

                if (drv != null)
                {
                    _selectedGrower = new Grower();
                    _selectedGrower.growerId = Int32.Parse(drv.Row[0].ToString());
                    _selectedGrower.name = drv.Row[1].ToString();
                }
                else
                {
                    //MessageBox.Show("선택하신 생산자가 없습니다.", "생산자 선택오류", MessageBoxButton.OK);
                }
            }
            else {
                MessageBox.Show("시스템 오류입니다. 관리자에게 문의하세요.");
            }
            this.Close();
            /*
            //search the object hierarchy for a datagrid row
            DependencyObject source = (DependencyObject)e.OriginalSource;
            DataGridRow row = UIHelpers.TryFindParent<DataGridRow>(source);

            //the user did not click on a row
            if (row == null) return;

            //[insert great code here...]
            // do your stuff
            DataRowView drv = (DataRowView)row.Item;
             * */
           // this.SelectedGrower = new Grower(drv.Row[0].ToString(), drv.Row[5].ToString(), drv.Row[1].ToString(), drv.Row[2].ToString(), drv.Row[2].ToString());
           
            
        }
    }
}
