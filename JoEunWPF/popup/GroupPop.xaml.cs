﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.ComponentModel;
using JoEunWPF.Common;

namespace JoEunWPF.popup
{
    /// <summary>
    /// Interaction logic for GroupPop.xaml
    /// </summary>
    public partial class GroupPop : Window, INotifyPropertyChanged
    {
        // update 여부
        public bool isUpdated = false;
        // 선택한 그룹
        private GroupModel SelectedGroup;

        // viewsource
        private CollectionViewSource groupViewSource;
        // dataset
        private JoEunDataSet joEunDataSet;
        private JoEunDataSetTableAdapters.GrowerGroupTableAdapter joEunDataSetGrowerGroupTableAdapter;

        public GroupPop()
        {
            InitializeComponent();
            double height = SystemParameters.WorkArea.Height;
            double width = SystemParameters.WorkArea.Width;
            this.Top = (height - this.Height) / 2;
            this.Left = (width - this.Width) / 2;   
            // data context 설정
            this.DataContext = this;
            SelectedGroup = new GroupModel();
            groupInfo.DataContext = SelectedGroup;
            joEunDataSet = ((JoEunWPF.JoEunDataSet)(this.FindResource("JoEunDataSet")));
            // Load data into the table Grower. You can modify this code as needed.
            joEunDataSetGrowerGroupTableAdapter = new JoEunWPF.JoEunDataSetTableAdapters.GrowerGroupTableAdapter();
        }

        // 윈도우 로드
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            searchGroup();
        }

        // 그룹목록 조회
        private void searchGroup()
        {
            joEunDataSetGrowerGroupTableAdapter.Fill(joEunDataSet.GrowerGroup);
            groupViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("groupViewSource")));
            groupViewSource.View.MoveCurrentToFirst();
            groupDataGrid.UnselectAll();
        }

        // 그룹 그리드 클릭
        private void groupClick(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                DataGridRow row = sender as DataGridRow;
                DataRowView drv = (DataRowView)row.Item;

                SelectedGroup = new GroupModel();
                SelectedGroup.GroupId = Int32.Parse(drv.Row[0].ToString());
                SelectedGroup.GroupName = drv.Row[1].ToString();
                SelectedGroup.Description = drv.Row[2].ToString();
                groupInfo.DataContext = SelectedGroup;
            }
            else
            {
                MessageBox.Show("시스템 오류입니다. 관리자에게 문의하세요.");
            }
        }

        // 그룹등록
        private void insertGroup(Object sender, RoutedEventArgs e)
        {
            // validation
            if (string.IsNullOrEmpty(SelectedGroup.GroupName))
            {
                MessageBox.Show("그룹명을 입력해주세요.", "그룹정보 누락", MessageBoxButton.OK);
                return;
            }
            MessageBoxResult result = MessageBox.Show("새로운 그룹을 저장하시겠습니까?", "그룹정보 등록", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.Cancel)
            {
                return;
            }

            var resultCount = joEunDataSetGrowerGroupTableAdapter.Insert(SelectedGroup.GroupName, SelectedGroup.Description);

            if (resultCount > 0)
            {
                // 재조회
                MessageBox.Show("저장되었습니다.", "그룹정보 등록완료", MessageBoxButton.OK);
                SelectedGroup = new GroupModel();
                groupInfo.DataContext = SelectedGroup;
                searchGroup();
                isUpdated = true;
            }
            else
            {
                MessageBox.Show("시스템 오류가 발생하였습니다. 동일증상 반복 발생시 시스템 관리자에게 문의하십시요.", "오류", MessageBoxButton.OK);
            }
        }
        // 그룹수정
        private void modifyGroup(Object sender, RoutedEventArgs e)
        {
            // validation
            if (SelectedGroup.GroupId < 1)
            {
                MessageBox.Show("수정하실 그룹을 선택해주세요.", "그룹정보 누락", MessageBoxButton.OK);
                return;
            }
            if (string.IsNullOrEmpty(SelectedGroup.GroupName))
            {
                MessageBox.Show("그룹명을 입력해주세요.", "그룹정보 누락", MessageBoxButton.OK);
                return;
            }
            MessageBoxResult result = MessageBox.Show("새로운 그룹을 저장하시겠습니까?", "그룹정보 수정", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.Cancel)
            {
                return;
            }

            var resultCount = joEunDataSetGrowerGroupTableAdapter.UpdateGroup(SelectedGroup.GroupName, SelectedGroup.Description, SelectedGroup.GroupId);

            if (resultCount > 0)
            {
                // 재조회
                MessageBox.Show("변경되었습니다.", "그룹정보 수정완료", MessageBoxButton.OK);
                SelectedGroup = new GroupModel();
                groupInfo.DataContext = SelectedGroup;
                searchGroup();
                isUpdated = true;
            }
            else
            {
                MessageBox.Show("시스템 오류가 발생하였습니다. 동일증상 반복 발생시 시스템 관리자에게 문의하십시요.", "오류", MessageBoxButton.OK);
            }
        }
        // 그룹삭제
        private void deleteGroup(Object sender, RoutedEventArgs e)
        {
            if (SelectedGroup.GroupId < 1)
            {
                MessageBox.Show("삭제하실 그룹을 선택해주세요.", "그룹정보 누락", MessageBoxButton.OK);
                return;
            }

            MessageBoxResult result = MessageBox.Show("선택하신 그룹을 삭제하시겠습니까?", "그룹삭제", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.Cancel)
            {
                return;
            }

            var resultCount = joEunDataSetGrowerGroupTableAdapter.DeleteGroup(SelectedGroup.GroupId);

            if (resultCount > 0)
            {
                // 재조회
                MessageBox.Show("삭제되었습니다.", "그룹삭제완료", MessageBoxButton.OK);
                SelectedGroup = new GroupModel();
                groupInfo.DataContext = SelectedGroup;
                searchGroup();
                isUpdated = true;
            }
            else
            {
                MessageBox.Show("시스템 오류가 발생하였습니다. 동일증상 반복 발생시 시스템 관리자에게 문의하십시요.", "오류", MessageBoxButton.OK);
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
