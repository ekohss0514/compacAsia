﻿namespace JoEunWPF
{
    using System;
    using System.Collections.Generic;
    
    public class GrowerModel
    {
        public GrowerModel()
        {

        }

        public int growerId { get; set; }
        public string growerCode { get; set; }
        public int groupId { get; set; }
        public string name { get; set; }
        public string tel { get; set; }
        public string description { get; set; }
    }
}
