﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace JoEunWPF.Common
{
    public class VentInfoModel : INotifyPropertyChanged
    {
        // 생산자 코드
        private int _growerId;
        public int GrowerId 
        {
            get { return _growerId; }
            set
            {
                _growerId = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("GrowerId"));
                }
            }
        }
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }
        // Grade A
        private int _a_xxl;
        public int A_XXL
        {
            get { return _a_xxl; }
            set
            {
                _a_xxl = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("A_XXL"));
                }
            }
        }
        private int _a_xxl_opc;
        public int A_XXL_OPC
        {
            get { return _a_xxl_opc; }
            set
            {
                _a_xxl_opc = value;
                A_XXL = _a_xxl_opc + A_XXL_Offset;
            }
        }
        private int _a_xxl_offset;
        public int A_XXL_Offset
        {
            get { return _a_xxl_offset; }
            set
            {
                _a_xxl_offset = value;
                A_XXL = A_XXL_OPC + _a_xxl_offset;
            }
        }
        private int _a_xl;
        public int A_XL
        {
            get { return _a_xl; }
            set
            {
                _a_xl = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("A_XL"));
                }
            }
        }
        private int _a_xl_opc;
        public int A_XL_OPC
        {
            get { return _a_xl_opc; }
            set
            {
                _a_xl_opc = value;
                A_XL = _a_xl_opc + A_XL_Offset;
            }
        }
        private int _a_xl_offset;
        public int A_XL_Offset
        {
            get { return _a_xl_offset; }
            set
            {
                _a_xl_offset = value;
                A_XL = A_XL_OPC + _a_xl_offset;
            }
        }
        private int _a_l;
        public int A_L
        {
            get { return _a_l; }
            set
            {
                _a_l = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("A_L"));
                }
            }
        }
        private int _a_l_opc;
        public int A_L_OPC
        {
            get { return _a_l_opc; }
            set
            {
                _a_l_opc = value;
                A_L = _a_l_opc + A_L_Offset;
            }
        }
        private int _a_l_offset;
        public int A_L_Offset
        {
            get { return _a_l_offset; }
            set
            {
                _a_l_offset = value;
                A_L = A_L_OPC + _a_l_offset;
            }
        }
        private int _a_m;
        public int A_M
        {
            get { return _a_m; }
            set
            {
                _a_m = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("A_M"));
                }
            }
        }
        private int _a_m_opc;
        public int A_M_OPC
        {
            get { return _a_m_opc; }
            set
            {
                _a_m_opc = value;
                A_M = _a_m_opc + A_M_Offset;
            }
        }
        private int _a_m_offset;
        public int A_M_Offset
        {
            get { return _a_m_offset; }
            set
            {
                _a_m_offset = value;
                A_M = A_M_OPC + _a_m_offset;
            }
        }
        private int _a_s;
        public int A_S
        {
            get { return _a_s; }
            set
            {
                _a_s = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("A_S"));
                }
            }
        }
        private int _a_s_opc;
        public int A_S_OPC
        {
            get { return _a_s_opc; }
            set
            {
                _a_s_opc = value;
                A_S = _a_s_opc + A_S_Offset;
            }
        }
        private int _a_s_offset;
        public int A_S_Offset
        {
            get { return _a_s_offset; }
            set
            {
                _a_s_offset = value;
                A_S = A_S_OPC + _a_s_offset;
            }
        }
        private int _a_ss;
        public int A_SS
        {
            get { return _a_ss; }
            set
            {
                _a_ss = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("A_SS"));
                }
            }
        }
        private int _a_ss_opc;
        public int A_SS_OPC
        {
            get { return _a_ss_opc; }
            set
            {
                _a_ss_opc = value;
                A_SS = _a_ss_opc + A_SS_Offset;
            }
        }
        private int _a_ss_offset;
        public int A_SS_Offset
        {
            get { return _a_ss_offset; }
            set
            {
                _a_ss_offset = value;
                A_SS = A_SS_OPC + _a_ss_offset;
            }
        }
        // Grade B
        private int _b_xxl;
        public int B_XXL
        {
            get { return _b_xxl; }
            set
            {
                _b_xxl = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("B_XXL"));
                }
            }
        }
        private int _b_xxl_opc;
        public int B_XXL_OPC
        {
            get { return _b_xxl_opc; }
            set
            {
                _b_xxl_opc = value;
                B_XXL = _b_xxl_opc + B_XXL_Offset;
            }
        }
        private int _b_xxl_offset;
        public int B_XXL_Offset
        {
            get { return _b_xxl_offset; }
            set
            {
                _b_xxl_offset = value;
                B_XXL = B_XXL_OPC + _b_xxl_offset;
            }
        }
        private int _b_xl;
        public int B_XL
        {
            get { return _b_xl; }
            set
            {
                _b_xl = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("B_XL"));
                }
            }
        }
        private int _b_xl_opc;
        public int B_XL_OPC
        {
            get { return _b_xl_opc; }
            set
            {
                _b_xl_opc = value;
                B_XL = _b_xl_opc + B_XL_Offset;
            }
        }
        private int _b_xl_offset;
        public int B_XL_Offset
        {
            get { return _b_xl_offset; }
            set
            {
                _b_xl_offset = value;
                B_XL = B_XL_OPC + _b_xl_offset;
            }
        }
        private int _b_l;
        public int B_L
        {
            get { return _b_l; }
            set
            {
                _b_l = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("B_L"));
                }
            }
        }
        private int _b_l_opc;
        public int B_L_OPC
        {
            get { return _b_l_opc; }
            set
            {
                _b_l_opc = value;
                B_L = _b_l_opc + B_L_Offset;
            }
        }
        private int _b_l_offset;
        public int B_L_Offset
        {
            get { return _b_l_offset; }
            set
            {
                _b_l_offset = value;
                B_L = B_L_OPC + _b_l_offset;
            }
        }
        private int _b_m;
        public int B_M
        {
            get { return _b_m; }
            set
            {
                _b_m = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("B_M"));
                }
            }
        }
        private int _b_m_opc;
        public int B_M_OPC
        {
            get { return _b_m_opc; }
            set
            {
                _b_m_opc = value;
                B_M = _b_m_opc + B_M_Offset;
            }
        }
        private int _b_m_offset;
        public int B_M_Offset
        {
            get { return _b_m_offset; }
            set
            {
                _b_m_offset = value;
                B_M = B_M_OPC + _b_m_offset;
            }
        }
        private int _b_s;
        public int B_S
        {
            get { return _b_s; }
            set
            {
                _b_s = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("B_S"));
                }
            }
        }
        private int _b_s_opc;
        public int B_S_OPC
        {
            get { return _b_s_opc; }
            set
            {
                _b_s_opc = value;
                B_S = _b_s_opc + B_S_Offset;
            }
        }
        private int _b_s_offset;
        public int B_S_Offset
        {
            get { return _b_s_offset; }
            set
            {
                _b_s_offset = value;
                B_S = B_S_OPC + _b_s_offset;
            }
        }
        private int _b_ss;
        public int B_SS
        {
            get { return _b_ss; }
            set
            {
                _b_ss = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("B_SS"));
                }
            }
        }
        private int _b_ss_opc;
        public int B_SS_OPC
        {
            get { return _b_ss_opc; }
            set
            {
                _b_ss_opc = value;
                B_SS = _b_ss_opc + B_SS_Offset;
            }
        }
        private int _b_ss_offset;
        public int B_SS_Offset
        {
            get { return _b_ss_offset; }
            set
            {
                _b_ss_offset = value;
                B_SS = B_SS_OPC + _b_ss_offset;
            }
        }
        // Grade C
        private int _c_xxl;
        public int C_XXL
        {
            get { return _c_xxl; }
            set
            {
                _c_xxl = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("C_XXL"));
                }
            }
        }
        private int _c_xxl_opc;
        public int C_XXL_OPC
        {
            get { return _c_xxl_opc; }
            set
            {
                _c_xxl_opc = value;
                C_XXL = _c_xxl_opc + C_XXL_Offset;
            }
        }
        private int _c_xxl_offset;
        public int C_XXL_Offset
        {
            get { return _c_xxl_offset; }
            set
            {
                _c_xxl_offset = value;
                C_XXL = C_XXL_OPC + _c_xxl_offset;
            }
        }
        private int _c_xl;
        public int C_XL
        {
            get { return _c_xl; }
            set
            {
                _c_xl = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("C_XL"));
                }
            }
        }
        private int _c_xl_opc;
        public int C_XL_OPC
        {
            get { return _c_xl_opc; }
            set
            {
                _c_xl_opc = value;
                C_XL = _c_xl_opc + C_XL_Offset;
            }
        }
        private int _c_xl_offset;
        public int C_XL_Offset
        {
            get { return _c_xl_offset; }
            set
            {
                _c_xl_offset = value;
                C_XL = C_XL_OPC + _c_xl_offset;
            }
        }
        private int _c_l;
        public int C_L
        {
            get { return _c_l; }
            set
            {
                _c_l = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("C_L"));
                }
            }
        }
        private int _c_l_opc;
        public int C_L_OPC
        {
            get { return _c_l_opc; }
            set
            {
                _c_l_opc = value;
                C_L = _c_l_opc + C_L_Offset;
            }
        }
        private int _c_l_offset;
        public int C_L_Offset
        {
            get { return _c_l_offset; }
            set
            {
                _c_l_offset = value;
                C_L = C_L_OPC + _c_l_offset;
            }
        }
        private int _c_m;
        public int C_M
        {
            get { return _c_m; }
            set
            {
                _c_m = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("C_M"));
                }
            }
        }
        private int _c_m_opc;
        public int C_M_OPC
        {
            get { return _c_m_opc; }
            set
            {
                _c_m_opc = value;
                C_M = _c_m_opc + C_M_Offset;
            }
        }
        private int _c_m_offset;
        public int C_M_Offset
        {
            get { return _c_m_offset; }
            set
            {
                _c_m_offset = value;
                C_M = C_M_OPC + _c_m_offset;
            }
        }
        private int _c_s;
        public int C_S
        {
            get { return _c_s; }
            set
            {
                _c_s = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("C_S"));
                }
            }
        }
        private int _c_s_opc;
        public int C_S_OPC
        {
            get { return _c_s_opc; }
            set
            {
                _c_s_opc = value;
                C_S = _c_s_opc + C_S_Offset;
            }
        }
        private int _c_s_offset;
        public int C_S_Offset
        {
            get { return _c_s_offset; }
            set
            {
                _c_s_offset = value;
                C_S = C_S_OPC + _c_s_offset;
            }
        }
        private int _c_ss;
        public int C_SS
        {
            get { return _c_ss; }
            set
            {
                _c_ss = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("C_SS"));
                }
            }
        }
        private int _c_ss_opc;
        public int C_SS_OPC
        {
            get { return _c_ss_opc; }
            set
            {
                _c_ss_opc = value;
                C_SS = _c_ss_opc + C_SS_Offset;
            }
        }
        private int _c_ss_offset;
        public int C_SS_Offset
        {
            get { return _c_ss_offset; }
            set
            {
                _c_ss_offset = value;
                C_SS = C_SS_OPC + _c_ss_offset;
            }
        }
        // Grade D
        private int _d;
        public int D
        {
            get { return D_OPC + D_Offset; }
            set
            {
                _d = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("D"));
                }
            }
        }
        private int _d_opc;
        public int D_OPC
        {
            get { return _d_opc; }
            set
            {
                _d_opc = value;
                D = _c_ss_opc + D_Offset;
            }
        }
        private int _d_offset;
        public int D_Offset
        {
            get { return _d_offset; }
            set
            {
                _d_offset = value;
                D = D_OPC + _d_offset;
            }
        }

        public VentInfoModel()
        {
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
