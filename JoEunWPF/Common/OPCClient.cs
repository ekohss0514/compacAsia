﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AxLGEOCXLib;

namespace JoEunWPF.Common
{
    public class OPCCLient : AxLGEOCX //Create LGEOCX Wrapper class
    {
        // constructor
        public OPCCLient()
        {
            CreateControl();
        }

        // 메모리 read
        public Object readWord(String deviceName, String address, int nCount)
        {
            short wType, ret;
            Object wQuality;
            Object Timestamp;
            Object nData;
            wType = 18;//VT_UI2
            Timestamp = nData = wQuality = null;

            ret = ReadData2(deviceName, address, wType, nCount, ref Timestamp, ref wQuality, ref nData);

            if (ret > 0)
            {
                //읽기 성공
                return nData;
            }
            return null;
        }

        // 메모리 write
        public void writeWord(String deviceName, String address, int nCount, int value)
        {
            int i;
            short wType, ret;
            Object nData;
            short[] shData;
            wType = 18;//2;//VT_I2

            if (nCount < 1)
            {
                return;
            }
            else if (nCount == 1)
            {
                nData = (Object)value;
            }
            else
            {

                shData = new short[nCount];
                for (i = 0; i < nCount; i++)
                {
                    shData[i] = (short)(i + value);
                }
                nData = (object)shData;
            }
            ret = WriteData(deviceName, address, wType, nCount, nData);
            if (ret > 0)
            {
                //쓰기 성공
            }
        }
    }
}
